// Які існують типи даних у Javascript?
//   - 1 Типы данных бывают строчные, числовые, булевые, null, undefined, BigInt, object, symbol, NaN
// У чому різниця між == і ===?
    //  - == не строгое сравнение где оператор равенства приводит обе величины к общему
     // типу, а === это строгое равно, где сравниваются типы и значения переменных.
     
// Що таке оператор?
    // - это возможность присвоить что-то в языке JS записть какие-то данные и дальше использовать их

//     let name = prompt("Enter your name", "");
//     let age = parseInt(prompt('Enter your age', ""), 10);

// while (!name || isNaN(age)) {
//     name = prompt("Please enter your name", name);
//     age = parseInt(prompt("Please enter your age", age), 10);
// }

// if (age < 18) {
//     alert("You are not allowed to visit this website");
// } else if (age >= 18 && age <= 22) {
//     let result = confirm("Are you sure you want to continue?");
//     if (result) {
//         alert(`Welcome, ${name}!`);
//     } else {
//         alert("You are not allowed to visit this website");
//     } 
// } else {
//     alert(`Welcome, ${name}!`);
// }
    

let name = prompt("Enter your name:");
let age = parseInt(prompt("Enter your age:"));

while (!name || isNaN(age)) {
  name = prompt(`Enter your name again (previous value: "${name}"):`);
  age = parseInt(prompt(`Enter your age again (previous value: "${age}"):`));
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${name}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome, ${name}`);
}

